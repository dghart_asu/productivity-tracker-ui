import React from 'react';
import { withCookies } from 'react-cookie';
import { Alert, Typography, notification } from 'antd';
import styled from 'styled-components';

import TrackerApiClient from '../services/TrackerApiClient';

const { Text } = Typography;

const StyledBox = styled.div`
    margin: 20px 0;
`;

class PendingJoinRequests extends React.Component {
    constructor(props) {
        super(props);

        this.apiClient = new TrackerApiClient(this.props.cookies);

        this.state = {
            alerts: []
        }
    }

    componentDidMount = async () => {
       this.fetchJoinRequests();
    }

    async fetchJoinRequests() {
        try {
            const response = await this.apiClient.getJoinRequests();
            const newAlerts = response.data.map( a => {
                return {
                   email: a.user.email,
                   workspaceName: a.workspace.name,
                   workspaceCode: a.workspace.code,
                   requestId: a.id
               };
            });

            this.setState({alerts: newAlerts});
        } catch (error) {
            this.handleError(error);
        }
    }

    handleError(error) {
        let description = '';
        if(error.response === undefined) {
            description = 'There was an error processing your request: ' + error;
        } else if (error.response.status >= 500) {
            description = 'There was an error while creating the workspace. Please try again.';
        } else {
            description = error.response.data.message;
        }

        notification.error({
            message: 'Error',
            description: description,
        });
    }

    async handleJoinRequest(id, answer) {
        this.apiClient.answerJoinRequest(id, answer).then((response) => {
            this.fetchJoinRequests();
        });
    }

    renderJoinRequest(data) {
        const msg = (
            <div>
                <div style={{display:"inline-block"}}>
                    {`${data.email} wants to join the "${data.workspaceName}" (${data.workspaceCode}) workspace`}
                </div>
                <div style={{display:"inline-block", float:"right"}}>
                    <a onClick={() => this.handleJoinRequest(data.requestId, true)} href="#">Accept</a> |
                    <a onClick={() => this.handleJoinRequest(data.requestId, false)}><Text type="danger"> Reject</Text></a>
                </div>
            </div>
        );

        return (
            <Alert key={data.requestId} style={{marginBottom:"3px"}} message={msg} type="info" banner />
        );
    }

    render() {
        const { alerts } = this.state;

        // Only show the box if there are alerts
        if(alerts.length < 1) {
            return null;
        }

        return (
            <StyledBox>
                { alerts.map( a => this.renderJoinRequest(a) ) }
            </StyledBox>
        );
    }
}

export default withCookies(PendingJoinRequests);
