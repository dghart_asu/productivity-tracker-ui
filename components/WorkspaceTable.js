import React from 'react';
import { withCookies } from 'react-cookie';
import { withRouter } from 'next/router';
import styled from 'styled-components';
import PropTypes from 'prop-types';

import { Table, Modal, notification } from 'antd';
import { ExclamationCircleOutlined } from '@ant-design/icons';

import { StyledPtButton } from '.'

import TrackerApiClient from '../services/TrackerApiClient';


const StyledTable = styled.table`
    width: 100%;

    & thead {
        background-color: darkgray
    }

    & thead tr th {
        text-align: left;
        padding: 2px 5px;
    }

    & thead tr th:last-child {
        width: 25%
    }

    & tbody tr:nth-child(odd) {
        background-color: #DDD;
    }
`;

const StyledRow = styled.tr`
    background-color: #CCC;

    & td {
        padding: 2px 5px;
    }
`;

const createButton = (key, text, type, callback) => (
    <StyledPtButton
        key={key}
        type={type}
        onClick={callback}
        disabled={text === 'Pending'}
    >
        {text}
    </StyledPtButton>
);

class WorkspaceTable extends React.Component {
    constructor(props) {
        super(props);

        this.apiClient = new TrackerApiClient(this.props.cookies);

        this.state = {
        }
    }

    handleLeave(workspaceId, onClose) {
        this.apiClient.leaveWorkspace(workspaceId).then(
            (res) => {
                notification.info({
                    message: 'Success',
                    description: 'You have been removed from the workspace',
                });
                this.props.onChange();
            }
        ).catch(
            (res) => {
                const msg = (res.response.status < 500) ?
                res.response.data.message :
                'There was a error reaching the server. Please try again soon.';

                notification.error({
                    message: 'Error',
                    description: msg,
                });
            }
        );
        onClose();
    }

    handleDelete(workspaceId, onClose) {
        this.apiClient.deleteWorkspace(workspaceId).then(
            (res) => {
                notification.info({
                    message: 'Success',
                    description: 'You have deleted the workspace and all data.',
                });
                this.props.onChange();
            }
        ).catch(
            (res) => {
                const msg = (res.response.status < 500) ?
                res.response.data.message :
                'There was a error reaching the server. Please try again soon.';

                notification.error({
                    message: 'Error',
                    description: msg,
                });
            }
        );
        onClose();
    }

    createCallback(action, workspaceId) {
        switch (action) {
            case 'view':
                return () => { this.props.router.push(`/workspaces/${workspaceId}/reports`); }
                break;
            case 'leave':
                return () => {
                    Modal.confirm({
                        title: 'Leave Workspace',
                        content: 'Are you sure you want to leave this Workspace?',
                        icon: <ExclamationCircleOutlined />,
                        okType: 'danger',
                        onOk: (onClose) => { this.handleLeave(workspaceId, onClose); }
                    });
                }
            case 'delete':
                return () => {
                    Modal.confirm({
                        title: 'Delete Workspace',
                        content: 'Are you sure you want to delete this Workspace? ALL DATA WILL BE LOST!',
                        icon: <ExclamationCircleOutlined />,
                        okType: 'danger',
                        onOk: (onClose) => { this.handleDelete(workspaceId, onClose); }
                    });
                }
            break;
        }

        // default to doing nothing
        return () => {}
    }

    createRow(workspaceId, code, name, status) {
        const actions = [];

        if(status == "OWNER") {
            actions.push(createButton(0, 'View', 'primary', this.createCallback('view', workspaceId)))
            // TODO owners should be able to leave a workspace, but they are only allowed to do so if there
            // is another user already. Since there is no functionality to add a 2nd owner, I'm going to
            // leave this out to avoid confusion. The UI support already exists though.
            // actions.push(createButton(1, 'Leave', 'danger', this.createCallback('leave', workspaceId)))
            actions.push(createButton(2, 'Delete', 'danger', this.createCallback('delete', workspaceId)))
        }
        else if(status == "USER") {
            actions.push(createButton(0, 'View', 'primary', this.createCallback('view', workspaceId)))
            actions.push(createButton(1, 'Leave', 'danger', this.createCallback('leave', workspaceId)))
        }
        else if(status == "PENDING") {
             actions.push(createButton(1, 'Pending'))
        }

        return (
            <StyledRow key={workspaceId}>
                <td>{code}</td>
                <td>{name}</td>
                <td>{actions}</td>
            </StyledRow>
        );
    }

    render() {
        const { workspaces } = this.props;

        return (
           <StyledTable>
               <thead>
                   <tr>
                       <th>Join Code</th>
                       <th>Name</th>
                       <th>Actions</th>
                   </tr>
               </thead>
               <tbody>
                   { workspaces.map( w => this.createRow(w.id, w.code.toUpperCase(), w.name, w.status) )}
               </tbody>
           </StyledTable>
       );
    }
}

WorkspaceTable.propTypes = {
    workspaces: PropTypes.any.isRequired,
};

export default withRouter(withCookies(WorkspaceTable));