import React from 'react'
import { Redirect } from "react-router-dom";
import { withCookies } from 'react-cookie';
import { withRouter } from 'next/router'

class SecurePage extends React.Component {
    componentDidMount() {
        const { cookies } = this.props;
        const jwt = cookies.get('jwt');

        // The cookie should be removed in SecureCheck if it was invalid
        if(!jwt) {
            this.props.router.push('/login')
        }
    }

    render() {
        return null;
    }
}

export default withRouter(withCookies(SecurePage));