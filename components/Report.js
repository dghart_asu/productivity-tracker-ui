import { Divider, Empty } from 'antd';
import * as moment from 'moment'

const Report = (props) => {
    if (!props.report) return null;

    let {
        report,
        includeEmpty,
        includeHeader
    } = props;

    // default to true
    includeEmpty = (includeEmpty === undefined);
    includeHeader = (includeHeader === undefined);

    const {
        date,
        yesterday,
        today,
        blockers,
        notes,
        feedback
    } = report;

    const getValueOrDefault = (text) => {
        return text ?
            <p>{text}</p>
            :
            <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} description="N/A" />
    }

    return (
        <>
            { includeHeader ?
                <h2>Report for {moment(date, 'YYYY-MM-DD').format("dddd, LL")}</h2>
                : null
            }

            { includeEmpty || yesterday ?
                <>
                    <Divider orientation="left">Yesterday</Divider>
                    {getValueOrDefault(yesterday)}
                </> : null
            }

            { includeEmpty || today ?
                <>
                    <Divider orientation="left">Today</Divider>
                    {getValueOrDefault(today)}
                </> : null
            }

            { includeEmpty || blockers ?
                <>
                    <Divider orientation="left">Blockers</Divider>
                    {getValueOrDefault(blockers)}
                </> : null
            }

            { includeEmpty || notes ?
                <>
                    <Divider orientation="left">Notes</Divider>
                    {getValueOrDefault(notes)}
                </> : null
            }

            { includeEmpty || feedback ?
                <>
                    <Divider orientation="left">Feedback</Divider>
                    {getValueOrDefault(feedback)}
                </> : null
            }
        </>
    );
}

export default Report;