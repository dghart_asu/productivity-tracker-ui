import styled from 'styled-components';

export const Colors = {
    blue: 'blue',
    paleBlue: '#0a97f0',

    red: '#c40000',
    paleRed: '#f77777',

    gold: '#e0d100',
    paleGold: '#fff787',

    black: 'black',
    paleGrey: '#ababab',
};

export const FormContainer = styled.div`
    display: flex;
    flex-direction: column;
    width: 30%;
`;

export const ButtonContainer = styled.div`
    display: flex;
    justify-content: center;
    flex-direction: row;
`;
