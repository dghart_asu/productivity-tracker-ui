import React from 'react';
import Link from 'next/link';
import { withCookies } from 'react-cookie';
import { withRouter } from 'next/router'
import styled from 'styled-components';
import PropTypes from 'prop-types';

const StyledHeading = styled.div`
    margin: 20px 0;

    & a {
        margin-right: 15px
    }
`;


class SubMenu extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { workspaceId } = this.props;

        return (
            <StyledHeading>
                 <Link href={`/workspaces/${workspaceId}/reports`}>
                    <a title="Home">Reports</a>
                 </Link>
                 <Link href={`/workspaces/${workspaceId}/teams`}>
                    <a title="Teams">Teams</a>
                 </Link>
                 <Link href={`/workspaces/${workspaceId}/users`}>
                    <a title="Users">Users</a>
                 </Link>
            </StyledHeading>
        )
    }
}

SubMenu.propTypes = {
    workspaceId: PropTypes.string,
};


export default withRouter(SubMenu)