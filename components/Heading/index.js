import styled from 'styled-components';
import PropTypes from 'prop-types';

const StyledHeading = styled.div`
    display: flex;
    font-size: 20px;
`;

const Heading = (props) => (
    <StyledHeading>{props.children}</StyledHeading>
);

Heading.propTypes = {
    children: PropTypes.any.isRequired,
};

export default Heading;