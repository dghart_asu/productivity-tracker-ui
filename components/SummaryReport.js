import { Collapse, Divider, Empty } from 'antd';
const { Panel } = Collapse;

import * as moment from 'moment'

import ReportStack from './ReportStack'

const getValueOrDefault = (text) => {
    return text ?
        <p>{text}</p>
        :
        <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} description="N/A" />
}

const SummaryReport = (props) => {
    if (!props.summary) return null;

    const {
            date,
            publicNotes,
            //privateNotes
            reports
        } = props.summary;

    return <>
        <h2>Summary Report for {moment(date, 'YYYY-MM-DD').format("dddd, LL")}</h2>

        <Divider orientation="left">Notes</Divider>
        {getValueOrDefault(publicNotes)}

        { reports && <Divider orientation="left">Reports</Divider>}
        <ReportStack reports={reports} />
    </>
}

export default SummaryReport;