import styled from 'styled-components';
import PropTypes from 'prop-types';

import { Colors } from '../styles';

const errorStyle = `background-color: ${Colors.paleRed}`;
const warnStyle = `background-color: ${Colors.paleGold}`;
const infoStyle = `background-color: ${Colors.paleBlue}`;
const defaultStyle = `background-color: ${Colors.paleGrey}`;

const getType = ({type}) => {
    switch (type) {
        case 'error':
            return errorStyle;
        case 'warn':
            return warnStyle;
        case 'info':
            return infoStyle;
        default:
            return defaultStyle;
    }
};

const AlertStyle = styled.div`
    display: flex;
    flex-direction: row;
    font-size: 20px;
    border-radius: 4px;
    ${(props) => getType(props)};
    line-height: 20px;
    padding-left: 5px;
`;

const Alert = ({ type, message }) => (
    <AlertStyle type={type}>
        {message && message}
    </AlertStyle>    
);

Alert.propTypes = {
    message: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired,
};

export default Alert;