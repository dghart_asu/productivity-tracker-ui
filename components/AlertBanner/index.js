import styled from 'styled-components';
import PropTypes from 'prop-types';

import { Colors } from '../styles';

const errorStyle = `border: 1px solid ${Colors.red};
background-color: ${Colors.paleRed}`;
const warnStyle = `border: 1px solid ${Colors.gold};
background-color: ${Colors.paleGold}`;
const infoStyle = `border: 1px solid ${Colors.blue};
background-color: ${Colors.paleBlue}`;
const defaultStyle = `border: 1px sold ${Colors.black};
background-color: ${Colors.paleGrey}`

const getBannerType = ({type}) => {
    switch (type) {
        case 'error':
            return errorStyle;
        case 'warn':
            return warnStyle;
        case 'info':
            return infoStyle;
        default:
            return defaultStyle;
    }
};

const AlertBannerStyle = styled.div`
    display: flex;
    flex-direction: row;
    font-size: 20px;
    border-radius: 4px;
    ${(props) => getBannerType(props)};
    line-height: 20px;
    padding-left: 5px;
`;

const Title = styled.div`
    display: flex;
    font-weight: 500;
`;

const AlertBanner = ({ type, title, message }) => (
    <AlertBannerStyle type={type}>
        {title && <Title>{title}</Title>}
        {message && message}
    </AlertBannerStyle>    
);

AlertBanner.propTypes = {
    title: PropTypes.string,
    message: PropTypes.string,
    type: PropTypes.string.isRequired,
};

AlertBanner.defaultProps = {
    title: null,
    message: null,
};

export default AlertBanner;