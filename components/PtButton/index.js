import React from 'react';
import PropTypes from 'prop-types';
import { Button } from 'antd';

const PtButton = (props) => (
    <Button {...props}>{props.children}</Button>
);

PtButton.propTypes = {
    children: PropTypes.any.isRequired,
};

export default PtButton;