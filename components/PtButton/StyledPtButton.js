import styled from 'styled-components';
import { Button } from 'antd';
import PropTypes from 'prop-types';

import PtButton from './index';

import { Colors } from '../styles';

const primaryStyle = `background-color: ${Colors.paleBlue};
color: white`;

const dangerStyle = `background-color: ${Colors.paleRed};
color: black`;

const warningStyle = `background-color: ${Colors.paleGold};
color: black`;


const largeButton = `line-height: 20px;`;

const getColors = ({ type }) => {
    if (type === 'primary') {
        return primaryStyle;
    }
    if (type === 'danger') {
        return dangerStyle;
    }
    if (type === 'warning') {
        return warningStyle;
    }
}

const getSizes = ({ size }) => {
    switch (size) {
        case 'large':
            return largeButton;
        default:
            return 'line-height: 20px;'
    }
}

const PtButtonStyle = styled(Button)`
    margin: 0 3px;
`;

const StyledPtButton = (props) => (
    <PtButtonStyle {...props}></PtButtonStyle>
);

StyledPtButton.propTypes = {
    children: PropTypes.any.isRequired,
    type: PropTypes.string,
    size: PropTypes.string,
};

StyledPtButton.defaultProps = {
    type: null,
    size: null,
};

export default StyledPtButton;
