import { Collapse, Divider, Empty } from 'antd';
const { Panel } = Collapse;

import * as moment from 'moment'

import Report from './Report'

const renderReportPanel = (report, key) => {
    return (
        <Panel header={moment(report.date, 'YYYY-MM-DD').format("dddd -  LL")} key={key}>
          <Report report={report} includeHeader="false" includeEmpty="false" />
        </Panel>
    );
}

const ReportStack = (props) => {
    if (!props.reports) return null;

    const { reports } = props

    return <>
        { reports && reports.length && (
            <Collapse accordion>
                { reports.map( (r, i) => renderReportPanel(r, i) ) }
            </Collapse>)
        }
    </>
}

export default ReportStack;