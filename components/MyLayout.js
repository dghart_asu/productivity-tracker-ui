import React from 'react';
import { CookiesProvider } from 'react-cookie';
import { Alert } from 'antd';

import Header from './Header';
import SecureCheck from './SecureCheck';
import SubMenu from './SubMenu';
import PendingJoinRequests from './PendingJoinRequests';

const layoutStyle = {
  margin: 20,
  padding: 20
};

class Layout extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { workspaceId, children } = this.props;

        return (
            <CookiesProvider>
                <SecureCheck />
                <div style={layoutStyle}>
                    <Header />
                    { workspaceId ? <PendingJoinRequests /> : '' }
                    { workspaceId > 0 ? <SubMenu workspaceId={workspaceId} /> : '' }
                    { children }
                </div>
            </CookiesProvider>
        );
    }
}

export default Layout;