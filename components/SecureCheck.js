import { withCookies } from 'react-cookie';
import React from 'react'

import UserServiceClient from '../services/UserServiceClient';

class SecureCheck extends React.Component {
    componentDidMount() {
       const { cookies } = this.props;
       let jwt = cookies.get('jwt') ? cookies.get('jwt') : localStorage.getItem("jwt");

       // Remove the jwt cookie if it's not valid
       if(jwt) {
            // Make sure they're the same to avoid bugs
            cookies.set('jwt', jwt);
            localStorage.setItem("jwt", jwt);

            UserServiceClient.validate(JSON.stringify({jwt: jwt})).catch(err => {
               cookies.set('jwt', '');
               localStorage.setItem("jwt", '');
            });
       }
    }

    render() {
       return null;
    }
}

export default withCookies(SecureCheck);