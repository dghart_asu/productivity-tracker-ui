import React from 'react';
import Link from 'next/link';
import { withCookies } from 'react-cookie';
import { withRouter } from 'next/router'

const linkStyle = {
  marginRight: 15
};

const AnonymousMenu = () => (
<div>
    <div style={{display: 'inline-block', float: 'right'}}>
        <Link href="/">
              <a style={linkStyle} title="Home">Home</a>
        </Link>
        <Link href="/sign_up">
            <a style={linkStyle} title="Sign Up">Sign Up</a>
        </Link>
        <Link href="/login">
            <a style={linkStyle} title="Log In">Log In</a>
        </Link>
    </div>
    <Link href="/">
       <h1>Productivity Tracker</h1>
    </Link>
</div>
)

class UserMenu extends React.Component {
    constructor(props) {
        super(props);

        this.handleLogout = this.handleLogout.bind(this);
    }

    handleLogout() {
        const { cookies } = this.props;
        cookies.set('jwt', '');
        localStorage.setItem("jwt", '');
        this.props.router.push('/login')
    }

    render() {
        return (
            <div>
                <div style={{display: 'inline-block', float: 'right'}}>
                    <Link href="/workspaces">
                          <a style={linkStyle} title="Workspaces">Workspaces</a>
                    </Link>
                    <Link href="/account">
                            <a style={linkStyle} title="My Account">My Account</a>
                        </Link>
                    <a href="#" onClick={this.handleLogout} style={linkStyle} title="Log Out">Log Out</a>
                </div>
                <Link href="/workspaces">
                   <h1>Productivity Tracker</h1>
                </Link>
            </div>
        );
    }
}
UserMenu = withRouter(withCookies(UserMenu));

class Header extends React.Component {
    render() {
        const { cookies } = this.props;
        const jwt = cookies.get('jwt');

        return (
            <header>
                { jwt ? <UserMenu /> : <AnonymousMenu /> }
                <div style={{clear:'all'}}></div>
            </header>
        );
    }
}

export default withCookies(Header);