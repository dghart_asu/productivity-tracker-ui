import styled from 'styled-components';
import { Input } from 'antd';

const UnstyledInput = styled(Input)`
    display: flex;
    border: 1px solid black;
    margin: 10px 0 10px 0;
    width: 100%;
    line-height: 25px;
    border-radius: 4px;
    padding-left: 5px;
`;

const PtInput = (props) => (
    <UnstyledInput {...props}></UnstyledInput>
);

export default PtInput;