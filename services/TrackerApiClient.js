import configs from '../config'
import axios from 'axios'

let apiRoot = configs.trackerApiRoot;

class TrackerApiClient {
    constructor(cookies) {
        this.cookies = cookies;
    }

    async getWorkspaces() {
        return axios.get(apiRoot + 'ws/v1/workspaces', this.getHeaders())
    }

    async joinWorkspace(code) {
        const data = {
            "code": code
        };

        return axios.post(apiRoot + 'ws/v1/joinrequests', data, this.getHeaders())
    }

    async createWorkspace(code, name) {
        const data = {
            "code": code,
            "name": name
        };

        return axios.post(apiRoot + 'ws/v1/workspaces', data, this.getHeaders())
    }

    async leaveWorkspace(workspaceId) {
        return axios.delete(apiRoot + 'ws/v1/joinrequests/' + workspaceId, this.getHeaders())
    }

    async deleteWorkspace(workspaceId) {
        return axios.delete(apiRoot + 'ws/v1/workspaces/' + workspaceId, this.getHeaders())
    }

    async getJoinRequests() {
        return axios.get(apiRoot + 'ws/v1/joinrequests', this.getHeaders())
    }

    async answerJoinRequest(requestId, answer) {
        const data = {
            "id": requestId,
            "answer": answer
        };

        return axios.put(apiRoot + 'ws/v1/joinrequests', data, this.getHeaders());
    }

    async getSettings() {
        return axios.get(apiRoot + 'ws/v1/settings', this.getHeaders())
    }

    async updateSettings(oldPassword, newPassword, notifications) {
        const data = {
            "notifications": !!notifications,
        };

        if(oldPassword || newPassword) {
            data.oldPassword = oldPassword;
            data.newPassword = newPassword;
        }

        return axios.put(apiRoot + 'ws/v1/settings', data, this.getHeaders());
    }

    async getReports() {
        return axios.get(apiRoot + 'ws/v1/reports', this.getHeaders())
    }

    async getUserReports(workspaceId, userId) {
        return axios.get(apiRoot + 'ws/v1/workspaces/' + workspaceId + '/users/' + userId + '/reports', this.getHeaders())
    }

    async getReportsForWorkspace(workspaceId) {
        return axios.get(apiRoot + 'ws/v1/reports?workspace=' + workspaceId, this.getHeaders())
    }

    async getReport(reportId) {
        return axios.get(apiRoot + 'ws/v1/reports/' + reportId, this.getHeaders())
    }

    async createReport(data) {
        return axios.post(apiRoot + 'ws/v1/reports', data, this.getHeaders());
    }

    async getReportsInRange(workspaceId, startDate, endDate) {
        return axios.get(apiRoot +
            'ws/v1/summaries/preview?workspace=' + workspaceId +
            '&startDate=' + startDate +
            '&endDate=' + endDate,
            this.getHeaders())
    }

    async getSummaryReports() {
        return axios.get(apiRoot + 'ws/v1/summaries', this.getHeaders())
    }

    async getUserSummaryReports(workspaceId, userId) {
        return axios.get(apiRoot + 'ws/v1/workspaces/' + workspaceId + '/users/' + userId + '/summaries', this.getHeaders())
    }

    async getSummaryReportsForWorkspace(workspaceId) {
        return axios.get(apiRoot + 'ws/v1/summaries?workspace=' + workspaceId, this.getHeaders())
    }

    async getSummaryReport(summaryId) {
        return axios.get(apiRoot + 'ws/v1/summaries/' + summaryId, this.getHeaders())
    }

    async createSummaryReport(data) {
        return axios.post(apiRoot + 'ws/v1/summaries', data, this.getHeaders());
    }

    async getTeamsForWorkspace(workspaceId) {
        return axios.get(apiRoot + 'ws/v1/teams?workspace=' + workspaceId, this.getHeaders())
    }

    async getTeam(teamId) {
        return axios.get(apiRoot + 'ws/v1/teams/' + teamId, this.getHeaders())
    }

    async createTeam(workspaceId, teamName) {
        const data = {
            "workspace": workspaceId,
            "name": teamName
        };

        return axios.post(apiRoot + 'ws/v1/teams', data, this.getHeaders());
    }

    async deleteTeam(teamId) {
        return axios.delete(apiRoot + 'ws/v1/teams/' + teamId,this.getHeaders());
    }

    async addUserToTeam(teamId, userId, role) {
        const data = {
            "user": userId,
            "role": role
        };

        return axios.post(apiRoot + 'ws/v1/teams/' + teamId + '/users', data, this.getHeaders());
    }

    async removeUserFromTeam(teamId, userId, role) {
        return axios.delete(apiRoot + 'ws/v1/teams/' + teamId + '/users/' + userId,this.getHeaders());
    }

    async getPermissions() {
        return axios.get(apiRoot + 'ws/v1/permissions', this.getHeaders())
    }

    async getUser(workspaceId, userId) {
        return axios.get(apiRoot + 'ws/v1/workspaces/' + workspaceId + '/users/' + userId, this.getHeaders())
    }

    async getUsersForWorkspace(workspaceId) {
        return axios.get(apiRoot + 'ws/v1/workspaces/' + workspaceId + '/users', this.getHeaders())
    }

    async getUserForWorkspace(workspaceId, userId) {
        return axios.get(apiRoot + 'ws/v1/workspaces/' + workspaceId + '/users/' + userId, this.getHeaders())
    }

    async removeUserForWorkspace(workspaceId, userId) {
        return axios.delete(apiRoot + 'ws/v1/joinrequests?workspace=' +
            workspaceId + "&user=" + userId, this.getHeaders())
    }

    getHeaders() {
        // Get header with jwt for auth
        let headers = {
            headers: {
                Authorization: `Bearer ${this.getJwt()}`
            }
        }

        return headers;
    }

    getJwt() {
        return this.getCookie('jwt') || localStorage.get('jwt');
    }

    getCookie(key) {
        return this.cookies.get(key);
    }
}

export default TrackerApiClient;
