import fetch from 'isomorphic-unfetch';
import configs from '../config'

let apiRoot = configs.apiRoot;

class UserServiceClient {

    get(url) {
    }

    async post(uri, data) {
        const res = await fetch(
            apiRoot + uri,
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: data
            }
         );

        const json = await res.json();
        if(res.status < 200 || res.status >= 300) {
            // If the request wasn't successful, throwing the same response
            // as an exception makes it cleaner for the promise library to
            // handle the difference. Users of this class won't need to check
            // the status themselves.
            throw json;
        }
        return json;
    }

    async postStatusOnly(uri, data) {
        const res = await fetch(
            apiRoot + uri,
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: data
            }
         );

        if(res.status < 200 || res.status >= 300) {
            throw res.status;
        }
        return res.status;
    }

    register(firstName, lastName, email) {
        const data = {
            firstName: firstName,
            lastName: lastName,
            email: email
        };

        return this.post('ws/v1/users/register', JSON.stringify(data));
    }

    setPassword(token, password) {
        const data = {
            token: token,
            password: password
        }
        return this.postStatusOnly('ws/v1/users/setPassword', JSON.stringify(data));
    }

    resetPassword(email) {
        const data = {
            email: email
        }
        return this.postStatusOnly('ws/v1/users/resetPassword', JSON.stringify(data));
    }

    authenticate(email, password) {
        const data = {
            email: email,
            password: password
        };
        return this.post('ws/v1/users/authenticate', JSON.stringify(data));
    }

    validate(data) {
        return this.postStatusOnly('ws/v1/users/validate', data)
    }
}

export default new UserServiceClient();