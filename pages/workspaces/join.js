import React from 'react';
import { withCookies } from 'react-cookie';
import { withRouter } from 'next/router';

import Layout from '../../components/MyLayout';
import SecurePage from '../../components/SecurePage';

import TrackerApiClient from '../../services/TrackerApiClient';

import { Heading, PtInput, AlertBanner, Alert, StyledPtButton} from '../../components';

import { Form, Input, Button, notification } from 'antd';

const formLayout = {
  wrapperCol: { span: 8 },
};


class JoinWorkspacePage extends React.Component {
    constructor(props) {
        super(props);

        this.apiClient = new TrackerApiClient(this.props.cookies);
    }

    onCancel() {
        this.props.router.push('/workspaces')
    }

    async onSubmit(values) {
        try {
            const response = await this.apiClient.joinWorkspace(values.code);
            //TODO show dialog first
            this.props.router.push('/workspaces');
        } catch (error) {
            this.handleError(error);
        }
    }

    handleError(error) {
        let description = '';
        if(error.response === undefined) {
            description = 'There was an error processing your request: ' + error;
        } else if (error.response.status >= 500) {
            description = 'There was an error while creating the workspace. Please try again.';
        } else {
            description = error.response.data.message;
        }

        notification.error({
            message: 'Error',
            description: description,
        });
    }

    render() {
        return (
            <Layout>
                <SecurePage />
                <h2>Join Existing Workspace</h2>
                <Form
                    {...formLayout}
                    name="basic"
                    onFinish={(values) => this.onSubmit(values)}
                >
                    <p className="ant-col ant-col-8">
                        You can join an existing workspace by entering the workspace's code here.
                        The owner of the workspace will then review a notification that you want
                        to join. After they approve, then you can create reports in that workspace.
                    </p>

                    <Form.Item
                        name="code"
                        rules={[{
                            required: true,
                            message: "Please enter the unique code for the workspace. Ask the workspace owner if you don't know what it is."
                            }]}
                    >
                        <Input placeholder="WORKSPACE_CODE" />
                    </Form.Item>



                    <Form.Item>
                        <Button type="primary" htmlType="submit" className="login-form-button">Request Access</Button>
                        <span> </span>
                        <Button htmlType="button" onClick={ () => this.onCancel() } type="danger">Cancel</Button>
                    </Form.Item>
                </Form>
            </Layout>
        );
    }
}

export default withRouter(withCookies(JoinWorkspacePage));