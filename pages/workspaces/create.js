import React from 'react';
import { withCookies } from 'react-cookie';
import { withRouter } from 'next/router';

import Layout from '../../components/MyLayout';
import SecurePage from '../../components/SecurePage';

import TrackerApiClient from '../../services/TrackerApiClient';

import { Heading, PtInput, AlertBanner, Alert, StyledPtButton} from '../../components';

import { Form, Input, Button, notification } from 'antd';

const formLayout = {
  wrapperCol: { span: 8 },
};


class NewWorkspacePage extends React.Component {
    constructor(props) {
        super(props);

        this.apiClient = new TrackerApiClient(this.props.cookies);
    }

    onCancel() {
        this.props.router.push('/workspaces')
    }

    async onSubmit(values) {
        try {
            const response = await this.apiClient.createWorkspace(values.code, values.name);
            this.props.router.push('/workspaces/' + response.data.id + "/reports");
        } catch (error) {
            this.handleError(error);
        }
    }

    handleError(error) {
        let description = '';
        if(error.response === undefined) {
            description = 'There was an error processing your request: ' + error;
        } else if (error.response.status >= 500) {
            description = 'There was an error while creating the workspace. Please try again.';
        } else {
            description = error.response.data.message;
        }

        notification.error({
            message: 'Error',
            description: description,
        });
    }

    render() {
        return (
            <Layout>
                <SecurePage />
                <h2>Create New Workspace</h2>
                <Form
                    {...formLayout}
                    name="basic"
                    onFinish={(values) => this.onSubmit(values)}
                >
                    <Form.Item
                        name="name"
                        rules={[{ required: true, message: 'Please enter a name for the workspace' }]}
                    >
                        <Input placeholder="Workspace Name" />
                    </Form.Item>

                    <Form.Item
                        name="code"
                        rules={[{
                            required: true,
                            message: 'Please enter a unique code for the workspace. Must contain only letters, numbers, and "="'
                            }]}
                    >
                        <Input placeholder="WORKSPACE_CODE" />
                    </Form.Item>



                    <Form.Item>
                        <Button type="primary" htmlType="submit" className="login-form-button">Create</Button>
                        <span> </span>
                        <Button htmlType="button" onClick={ () => this.onCancel() } type="danger">Cancel</Button>
                    </Form.Item>
                </Form>
            </Layout>
        );
    }
}

export default withRouter(withCookies(NewWorkspacePage));