import React from 'react';
import { withCookies } from 'react-cookie';
import { withRouter } from 'next/router';

import Layout from '../../components/MyLayout';
import SecurePage from '../../components/SecurePage';

import TrackerApiClient from '../../services/TrackerApiClient';

import { StyledPtButton, Heading } from '../../components';
import WorkspaceTable from '../../components/WorkspaceTable';

import { Spin, Alert, notification } from 'antd';

class WorkspacesPage extends React.Component {
  constructor(props) {
    super(props);

    this.apiClient = new TrackerApiClient(this.props.cookies);

    this.state = {
      errorMsg: null,
      workspaces: null,
      isLoading: true,
    }
  }

  // On page load, get the workspaces for the user
  componentDidMount = async () => {
    this.fetchTableData();
  }

  // If the User doesn't have any workspaces, give them an option to join or create one
  renderNewUserBody = () => {
    return (
      <>
        <Heading>Get Started</Heading>
        <StyledPtButton onClick={() => this.navTo('joinWorkspace')}>
            Join An Existing Workspace
        </StyledPtButton>
        <StyledPtButton onClick={() => this.navTo('newWorkspace')}>
            Create A New Workspace
        </StyledPtButton>
      </>
    );
  }


  async fetchTableData() {
    try {
        const response = await this.apiClient.getWorkspaces();
        this.setState({workspaces: response.data, isLoading: false});
    } catch (error) {
        this.handleError(error);
    }
  }

  handleError(error) {
      let description = '';
      if(error.response === undefined) {
          description = 'There was an error processing your request: ' + error;
      } else if (error.response.status >= 500) {
          description = 'There was an error while creating the workspace. Please try again.';
      } else {
          description = error.response.data.message;
      }

      notification.error({
          message: 'Error',
          description: description,
      });
  }

  // If the user has workspaces, show them in a table
  renderWorkspacesTable = () => {
    const { workspaces } = this.state;

    return (
      <>
        <StyledPtButton type="primary" onClick={() => this.navTo('joinWorkspace')}>
            Join An Existing Workspace
        </StyledPtButton>
        <StyledPtButton onClick={() => this.navTo('newWorkspace')}>
            Create A New Workspace
        </StyledPtButton>
        <h2>Workspaces</h2>
        <WorkspaceTable workspaces={workspaces} onChange={ () => {
            this.setState({isLoading: true});
            this.fetchTableData();
        }} />
      </>
    );
  }

  renderLoading() {
    return (
        <Spin tip="Loading...">
            <Alert
              message="Loading"
              description="Please wait while the data is retrieved."
              type="info"
            />
          </Spin>
    );
  }

  renderBody = () => {
    const { workspaces } = this.state;

    if(workspaces && workspaces.length > 0) {
        return this.renderWorkspacesTable();
    } else {
        return this.renderNewUserBody();
    }
  }

  navTo(slug) {
    switch (slug) {
      case 'joinWorkspace':
        this.props.router.push('/workspaces/join');
        break;
      case 'newWorkspace':
        this.props.router.push('/workspaces/create');
        break;
    }
  }

  render() {
    const { errorMsg, isLoading } = this.state;

    return (
      <Layout workspaceId="-1">
          <SecurePage />
          {isLoading ? this.renderLoading() : this.renderBody() }
          {errorMsg && <h4>{errorMsg}</h4>}
      </Layout>
    );
  }
}

export default withRouter(withCookies(WorkspacesPage));