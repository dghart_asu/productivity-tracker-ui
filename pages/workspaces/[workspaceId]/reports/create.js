import React from 'react';
import { withCookies } from 'react-cookie';
import { withRouter } from 'next/router';

import TrackerApiClient from '../../../../services/TrackerApiClient';

import Layout from '../../../../components/MyLayout';
import SecurePage from '../../../../components/SecurePage';

import {
    Button,
    DatePicker,
    Form,
    Input,
    notification
} from 'antd';

const formLayout = {
  labelCol: { span: 2 },
  wrapperCol: { span: 8 },
};

const tailLayout = {
  wrapperCol: { offset: 2, span: 8 },
};

class CreateReportPage extends React.Component {
    constructor(props) {
        super(props)

        this.apiClient = new TrackerApiClient(this.props.cookies);
    }

    handleError(error) {
        let description = '';
        if(error.response === undefined) {
            description = 'There was an error processing your request: ' + error;
        } else if (error.response.status >= 500) {
            description = 'There was an error while creating the workspace. Please try again.';
        } else {
            description = error.response.data.message;
        }

        notification.error({
            message: 'Error',
            description: description,
        });
    }

    async onSubmit(values) {
        const { workspaceId } = this.props.router.query
        values.workspaceId = workspaceId;
        values.date = values.date.format("YYYY-MM-DD");

        try {
            const response = await this.apiClient.createReport(values);
            this.props.router.push('/workspaces/' + workspaceId + "/reports");
        } catch (error) {
            this.handleError(error);
        }
    }

    onCancel() {
        const { workspaceId } = this.props.router.query;
        this.props.router.push('/workspaces/' + workspaceId + '/reports');
    }

    render() {
        const { workspaceId } = this.props.router.query

        return (
            <Layout workspaceId={workspaceId}>
                <SecurePage />
                <h2>Create New Report</h2>
                <Form
                    {...formLayout}
                    name="createTeam"
                    onFinish={(values) => this.onSubmit(values)}
                >

                    <Form.Item
                        name="date"
                        label="Date"
                        rules={[{ required: true, message: 'You must choose a date for this report!' }]}
                    >
                        <DatePicker />
                    </Form.Item>

                    <Form.Item name="yesterday" label="Yesterday">
                        <Input.TextArea
                            rows={4}
                            placeholder="What did you do and accomplish yesterday (or the last day you worked)?"
                        />
                    </Form.Item>

                    <Form.Item name="today" label="Today">
                        <Input.TextArea
                            rows={4}
                            placeholder="What are you going to be working on today?"
                        />
                    </Form.Item>

                    <Form.Item name="blockers" label="Blockers">
                        <Input.TextArea
                            rows={4}
                            placeholder="Problems that you need help to continue your work"
                        />
                    </Form.Item>

                    <Form.Item name="notes" label="Notes">
                        <Input.TextArea
                            rows={4}
                            placeholder="Additional notes for yourself or manager"
                        />
                    </Form.Item>

                    <Form.Item name="feedback" label="Feedback">
                        <Input.TextArea
                            rows={4}
                            placeholder="Feedback for other people who you've worked with"
                        />
                    </Form.Item>

                    <Form.Item {...tailLayout}>
                        <Button type="primary" htmlType="submit" className="login-form-button">Create</Button>
                        <span> </span>
                        <Button htmlType="button" onClick={ () => this.onCancel() } type="danger">Cancel</Button>
                    </Form.Item>
                </Form>

            </Layout>
        );
    }
}

export default withRouter(withCookies(CreateReportPage));
