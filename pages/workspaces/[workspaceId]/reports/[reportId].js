import React from 'react';
import { withCookies } from 'react-cookie';
import { withRouter } from 'next/router';

import TrackerApiClient from '../../../../services/TrackerApiClient';

import Report from '../../../../components/Report';
import Layout from '../../../../components/MyLayout';
import SecurePage from '../../../../components/SecurePage';

import { Alert, Spin, notification } from 'antd';


class SingleReportPage extends React.Component {
    constructor(props) {
        super(props)

        this.apiClient = new TrackerApiClient(this.props.cookies);

        this.state = {
            report: null,
            isLoading: true
        };
    }

    async fetchReport() {
        const { reportId } = this.props.router.query
        if(!reportId) return;

        try {
            const response = await this.apiClient.getReport(reportId);
            this.setState({report:response.data, isLoading: false});
        } catch (error) {
            this.handleError(error);
        }
    }

    handleError(error) {
        let description = '';
        if(error.response === undefined) {
            description = 'There was an error processing your request: ' + error;
        } else if (error.response.status >= 500) {
            description = 'There was an error while getting the report. Please try again.';
        } else {
            description = error.response.data.message;
        }

        notification.error({
            message: 'Error',
            description: description,
        });
    }

    componentDidMount = async () => {
        this.fetchReport();
    }

    renderLoading() {
        return (
            <Spin tip="Loading...">
                <Alert
                    message="Loading"
                    description="Please wait while the data is retrieved."
                    type="info"
                />
            </Spin>
        );
    }

    render() {
        const { isLoading, report } = this.state
        const { workspaceId, reportId } = this.props.router.query


        if(reportId && isLoading) {
            this.fetchReport();
        }

        return (
            <Layout workspaceId={workspaceId}>
                <SecurePage />
                { report ? <Report report={report} /> : this.renderLoading() }
            </Layout>
        );
    }
}

export default withRouter(withCookies(SingleReportPage));