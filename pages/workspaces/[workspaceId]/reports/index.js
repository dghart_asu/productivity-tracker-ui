import React from 'react';
import { withCookies } from 'react-cookie';
import { withRouter } from 'next/router';
import * as moment from 'moment'

import Layout from '../../../../components/MyLayout';
import SecurePage from '../../../../components/SecurePage';

import TrackerApiClient from '../../../../services/TrackerApiClient';

import {
    Alert as LoadingAlert,
    Button,
    Empty,
    Spin,
    Table,
    notification
} from 'antd';

const btnStyle = {
    margin: "0 3px"
}

const getTitleForReport = (report) => {
    if(report.type == 'summary') {
        return 'Summary for ' + moment(report.startDate, 'YYYY-MM-DD').format("ddd L") +
            ' to ' + moment(report.endDate, 'YYYY-MM-DD').format("ddd L") + ' (created for ' +
            moment(report.date, 'YYYY-MM-DD').format("ddd L") + ')';
    } else {
        return 'Report for ' + moment(report.date, 'YYYY-MM-DD').format("ddd, L");
    }
}

class ReportsPage extends React.Component {
    constructor(props) {
        super(props)

        this.apiClient = new TrackerApiClient(this.props.cookies);

        this.state = {
            reports: [],
            isLoading: true
        };
    }

    async componentDidMount() {
        this.fetchData();
    }

    async fetchData() {
        const { workspaceId } = this.props.router.query
        if(!workspaceId) return;

        try {
            const response = await this.apiClient.getReportsForWorkspace(workspaceId);
            const response2 = await this.apiClient.getSummaryReportsForWorkspace(workspaceId);

            const reports = response.data.map( (r, index) => { return {
                key: index,
                type: 'report',
                ...r
            }});

            response2.data.forEach( (r, index) => { reports.push({
                key: reports.length + index,
                startDate: r.reports.reduce((min, p) => p.date < min ? p.date : min, r.reports[0].date),
                endDate: r.reports.reduce((max, p) => p.date > max ? p.date : max, r.reports[0].date),
                type: 'summary',
                ...r
            })});

            // sort most recent to least recent
            reports.sort((a, b) => {
                if (a.date > b.date) return -1;
                if (b.date > a.date) return 1;

                return 0;
            })

            this.setState({
                reports: reports,
                isLoading: false
            });
        } catch (error) {
            this.handleError(error);
        }
    }

    handleError(error) {
        console.log(error);
        let description = '';
        if(error.response === undefined) {
            description = 'There was an error processing your request: ' + error;
        } else if (error.response.status >= 500) {
            description = 'There was an error while getting your reports. Please try again.';
        } else {
            description = error.response.data.message;
        }

        notification.error({
            message: 'Error',
            description: description,
        });
    }

    renderLoading() {
        return (
            <Spin tip="Loading...">
                <LoadingAlert
                    message="Loading"
                    description="Please wait while the data is retrieved."
                    type="info"
                />
            </Spin>
        );
    }

    renderBody() {
        const { reports } = this.state;

        if(reports.length < 1) {
            return <Empty
                image={Empty.PRESENTED_IMAGE_SIMPLE}
                description="There are currently no reports for this workspace"
            />
        }

        const columns = [
          {
            title: 'Report',
            dataIndex: 'date',
            key: 'date',
            render: (text, record, index) => {
                return <span>{getTitleForReport(record)}</span>
            }
          },
          {
            title: 'Actions',
            dataIndex: 'date',
            key: 'actions',
            render: (text, record, index) => {
                let action;
                if(record.type == 'summary') {
                    action = () => this.navToSummary(record.id)
                } else {
                    action = () => this.navToReport(record.id)
                }

                return <Button type="primary" onClick={ action }>View</Button>
             }
          },
        ];

        return (
            <Table dataSource={reports} columns={columns} />
        );
    }

    navToReport(reportId) {
        const { workspaceId } = this.props.router.query;
        this.props.router.push('/workspaces/' + workspaceId + '/reports/' + reportId);
    }

    navToSummary(reportId) {
        const { workspaceId } = this.props.router.query;
        this.props.router.push('/workspaces/' + workspaceId + '/summaries/' + reportId);
    }

    navToCreateReport() {
        const { workspaceId } = this.props.router.query;
        this.props.router.push('/workspaces/' + workspaceId + '/reports/create');
    }

    navToCreateSummary() {
        const { workspaceId } = this.props.router.query;
        this.props.router.push('/workspaces/' + workspaceId + '/summaries/create');
    }

    render() {
        const { isLoading, reports } = this.state;
        const { workspaceId } = this.props.router.query;

        if(workspaceId && isLoading) {
            this.fetchData();
        }

        return (
            <Layout workspaceId={workspaceId}>
                <SecurePage />
                <div style={{margin:'10px 0'}}>
                    <Button style={btnStyle} type="primary" onClick={() => this.navToCreateReport()}>Create Report</Button>
                    <Button style={btnStyle} onClick={() => this.navToCreateSummary()}>Create Summary</Button>
                </div>
                <h2>Reports</h2>
                {isLoading ? this.renderLoading() : this.renderBody() }
            </Layout>
        );
    }
}

export default withRouter(withCookies(ReportsPage));