import React from 'react';
import { withCookies } from 'react-cookie';
import { withRouter } from 'next/router';

import Layout from '../../../../components/MyLayout';
import SecurePage from '../../../../components/SecurePage';

import TrackerApiClient from '../../../../services/TrackerApiClient';

import {
    Alert as LoadingAlert,
    Button,
    Checkbox,
    Empty,
    Modal,
    Select,
    Spin,
    Table,
    notification
} from 'antd';

import { ExclamationCircleOutlined } from '@ant-design/icons';

const btnStyle = {
    margin: "0 3px"
}

class UsersPage extends React.Component {
    constructor(props) {
        super(props)

        this.apiClient = new TrackerApiClient(this.props.cookies);

        this.state = {
            users: [],
            teamMap: {},
            permissions: [],
            isLoading: true
        };
    }

    async componentDidMount() {
        this.fetchData();
    }

    async fetchData() {
        const { workspaceId } = this.props.router.query
        if(!workspaceId) return;

        try {
            const responseUsers = await this.apiClient.getUsersForWorkspace(workspaceId);
            const responseTeams = await this.apiClient.getTeamsForWorkspace(workspaceId);
            const responsePermissions = await this.apiClient.getPermissions();

            const teamMap = {}
            responseTeams.data.forEach(t => teamMap[t.id] = t.name);

            const users = responseUsers.data.map( (u, index) => { return {
                            key: index,
                            ...u
                        }});

            this.setState({
                users: users,
                teamMap: teamMap,
                permissions: responsePermissions.data,
                isLoading: false
            });
        } catch (error) {
            this.handleError(error);
        }
    }

    handleError(error) {
        let description = '';
        if(error.response === undefined) {
            description = 'There was an error processing your request: ' + error;
        } else if (error.response.status >= 500) {
            description = 'There was an error while getting users. Please try again.';
        } else {
            description = error.response.data.message;
        }

        notification.error({
            message: 'Error',
            description: description,
        });
    }

    renderLoading() {
        return (
            <Spin tip="Loading...">
                <LoadingAlert
                    message="Loading"
                    description="Please wait while the data is retrieved."
                    type="info"
                />
            </Spin>
        );
    }

    // unused
    // TODO add to render()
    // const isWorkspaceOwner = permissions.owner.indexOf(parseInt(workspaceId)) >= 0;
    // {isWorkspaceOwner ? this.renderCreateButton() : null }
    renderInviteButton() {
        return (
            <div style={{margin:'10px 0'}}>
                <Button type="primary" onClick={() => console.log('show invite modal')}>Invite User</Button>
            </div>
        );
    }

    navToUser(userId) {
        const { workspaceId } = this.props.router.query;
        this.props.router.push('/workspaces/' + workspaceId + '/users/' + userId);
    }

    navToTeam(teamId) {
        const { workspaceId } = this.props.router.query;
        this.props.router.push('/workspaces/' + workspaceId + '/teams/' + teamId);
    }

    showAddTeamModal(workspaceId, user) {
        const { teamMap } = this.state;

        const data = {
            team: null,
            isManager: false
        }

        const handleSubmit = async (onClose) => {
            if(data.team) {
                const role = data.isManager ? 'MANAGER' : 'MEMBER';
                try {
                    const response = await this.apiClient.addUserToTeam(data.team, user.id, role);
                    notification.info({
                        message: 'Success',
                        description: 'User has been added to the team',
                    });
                    onClose()
                    this.fetchData();
                } catch (error) {
                    this.handleError(error);
                }
            } else {
                notification.warning({
                    message: 'Warning',
                    description: 'Please Select a Team!',
                });
            }
        }

        Modal.confirm({
            title: 'Add User to Team',
            content: (
                <div>
                    <div style={{marginTop:'10px'}}>
                        <Select onChange={ v => {data.team = v}} style={{ width: 300 }} placeholder="Select a Team">
                            {teamMap && (
                               Object.keys(teamMap).map( (i) => (
                                <Select.Option key={i} value={i}>{teamMap[i]}</Select.Option>
                               ))
                            )}
                        </Select>
                    </div>
                    <div style={{marginTop:'10px'}}>
                        <Checkbox onChange={e => {data.isManager = e.target.checked}}>Assign as Manager</Checkbox>
                    </div>
                </div>

            ),
            okType: 'primary',
            onOk: (onClose) => { handleSubmit(onClose) }
        });
    }

    showRemoveModal(workspaceId, userId) {
        Modal.confirm({
            title: 'Remove User',
            content: 'Are you sure you want to remove this user from the workspace?',
            icon: <ExclamationCircleOutlined />,
            okType: 'danger',
            onOk: (onClose) => { this.handleRemove(workspaceId, userId, onClose); }
        });
    }

    async handleRemove(workspaceId, userId, onClose) {
        try {
            const response = await this.apiClient.removeUserForWorkspace(workspaceId, userId);
            notification.info({
                message: 'Success',
                description: 'User has been removed from the workspace',
            });
            this.fetchData();
        } catch (error) {
            this.handleError(error);
        }
        onClose();
    }

    renderBody() {
        const { users, teamMap, permissions } = this.state;
        const { workspaceId } = this.props.router.query;

        if(users.length < 1) {
            return <Empty
                image={Empty.PRESENTED_IMAGE_SIMPLE}
                description="There are currently no users for this workspace"
            />
        }

        const isWorkspaceOwner = permissions.owner.indexOf(parseInt(workspaceId)) >= 0;

        const columns = [
              {
                title: 'Email',
                dataIndex: 'email',
                key: 'email',
                render: (text, record, index) => {
                    return <a onClick={ () => this.navToUser(record.id) }>{text}</a>
                }
              },
              {
                title: 'Teams',
                key: 'teams',
                render: (text, record , index) => {
                    const teamNames = [];

                    text.permissions.teams.forEach( t => {
                        teamNames.push(
                            <a key={teamNames.length} onClick={ () => this.navToTeam(t) }>{teamMap[t]}</a>
                        );
                    });
                    text.permissions.manager.forEach( t => {
                        teamNames.push(
                            <a key={teamNames.length} onClick={ () => this.navToTeam(t) }>{teamMap[t]}*</a>
                        );
                    });

                    return (
                        <span>
                            {teamNames && teamNames
                                .map(t => <span key={t}>{t}</span>)
                                .reduce((accu, elem) => {
                                    return accu === null ? [elem] : [...accu, ', ', elem]
                                }, null)}
                        </span>
                    );
                }
              },
            ];

            if (isWorkspaceOwner) {
                columns.push(
                    {
                        title: 'Actions',
                        dataIndex: 'date',
                        key: 'actions',
                        render: (text, record, index) => {
                            return (
                                <>
                                    <Button style={btnStyle} type="primary" onClick={ () => this.showAddTeamModal(workspaceId, record) }>Add Team</Button>
                                    <Button style={btnStyle} type="danger" onClick={ () => this.showRemoveModal(workspaceId, record.id) }>Remove from Workspace</Button>
                                </>
                            );
                        }
                    },
                );
            }

        return (
            <>
            <Table dataSource={users} columns={columns} />
            <p>* User the the manager for this team</p>
            </>
        );
    }

    render() {
        const { isLoading } = this.state;
        const { workspaceId } = this.props.router.query;

        if(workspaceId && isLoading) {
            this.fetchData();
        }

        return (
            <Layout workspaceId={workspaceId}>
                <SecurePage />
                <h2>Users</h2>
                {isLoading ? this.renderLoading() : this.renderBody() }
            </Layout>
        );
    }
}

export default withRouter(withCookies(UsersPage));