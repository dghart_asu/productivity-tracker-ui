import React from 'react';
import { withCookies } from 'react-cookie';
import { withRouter } from 'next/router';
import * as moment from 'moment'

import TrackerApiClient from '../../../../services/TrackerApiClient';

import Report from '../../../../components/Report';
import Layout from '../../../../components/MyLayout';
import SecurePage from '../../../../components/SecurePage';

import { Alert, Button, Spin, Table, notification } from 'antd';

const getTitleForReport = (report) => {
    if(report.type == 'summary') {
        return 'Summary for ' + moment(report.startDate, 'YYYY-MM-DD').format("ddd L") +
            ' to ' + moment(report.endDate, 'YYYY-MM-DD').format("ddd L") + ' (created for ' +
            moment(report.date, 'YYYY-MM-DD').format("ddd L") + ')';
    } else {
        return 'Report for ' + moment(report.date, 'YYYY-MM-DD').format("ddd, L");
    }
}

class SingleUserPage extends React.Component {
    constructor(props) {
        super(props)

        this.apiClient = new TrackerApiClient(this.props.cookies);

        this.state = {
            user: null,
            teamMap: {},
            permissions: {},
            reports: [],
            isLoading: true
        };
    }

    async fetchData() {
        const { workspaceId, userId } = this.props.router.query
        if(!userId || !workspaceId) return;

        try {
            const response = await this.apiClient.getUser(workspaceId, userId);
            const responseTeams = await this.apiClient.getTeamsForWorkspace(workspaceId);
            const responsePermissions = await this.apiClient.getPermissions();

            const responseReports = await this.apiClient.getUserReports(workspaceId, userId);
            const responseSummaries = await this.apiClient.getUserSummaryReports(workspaceId, userId);

            const reports = responseReports.data.map( (r, index) => { return {
                key: index,
                type: 'report',
                ...r
            }});

            responseSummaries.data.forEach( (r, index) => { reports.push({
                key: reports.length + index,
                startDate: r.reports.reduce((min, p) => p.date < min ? p.date : min, r.reports[0].date),
                endDate: r.reports.reduce((max, p) => p.date > max ? p.date : max, r.reports[0].date),
                type: 'summary',
                ...r
            })});

            // sort most recent to least recent
            reports.sort((a, b) => {
                if (a.date > b.date) return -1;
                if (b.date > a.date) return 1;

                return 0;
            })

            const teamMap = {}
            responseTeams.data.forEach(t => teamMap[t.id] = t.name);

            this.setState({
                user: response.data,
                teamMap: teamMap,
                permissions: responsePermissions.data,
                reports: reports,
                isLoading: false
            });
        } catch (error) {
            this.handleError(error);
        }
    }

    handleError(error) {
        let description = '';
        if(error.response === undefined) {
            description = 'There was an error processing your request: ' + error;
        } else if (error.response.status >= 500) {
            description = 'There was an error while getting the user data. Please try again.';
        } else {
            description = error.response.data.message;
        }

        notification.error({
            message: 'Error',
            description: description,
        });
    }

    componentDidMount = async () => {
        this.fetchData();
    }

    navToReport(reportId) {
        const { workspaceId } = this.props.router.query;
        this.props.router.push('/workspaces/' + workspaceId + '/reports/' + reportId);
    }

    navToTeam(teamId) {
        const { workspaceId } = this.props.router.query;
        this.props.router.push('/workspaces/' + workspaceId + '/teams/' + teamId);
    }

    renderLoading() {
        return (
            <Spin tip="Loading...">
                <Alert
                    message="Loading"
                    description="Please wait while the data is retrieved."
                    type="info"
                />
            </Spin>
        );
    }

    renderTeamSummary(user) {
        const { teamMap } = this.state;

        const teams = [];
        user.permissions.teams.forEach( t => {
            teams.push(
                <li key={teams.length}>
                    <a onClick={ () => this.navToTeam(t)}>{teamMap[t]}</a>
                </li>
            );
        });
        user.permissions.manager.forEach( t => {
            teams.push(
                <li key={teams.length}>
                    <a onClick={ () => this.navToTeam(t)}>{teamMap[t]}</a> (Manager)
                </li>
            );
        });

        if(!teams.length) {
            return null;
        }

        return (
            <>
                <h2>Teams</h2>
                <ul>
                    {teams}
                </ul>
            </>
        );
    }

    renderReportList(reports) {
        const columns = [
          {
            title: 'Report',
            dataIndex: 'date',
            key: 'date',
            render: (text, record, index) => {
                return <span>{getTitleForReport(record)}</span>
            }
          },
          {
            title: 'Actions',
            dataIndex: 'date',
            key: 'actions',
            render: (text, record, index) => {
                let action;
                if(record.type == 'summary') {
                    action = () => this.navToSummary(record.id)
                } else {
                    action = () => this.navToReport(record.id)
                }

                return <Button type="primary" onClick={ action }>View</Button>
             }
          },
        ];

        if(!reports || !reports.length) {
            return null;
        }

        return (
            <>
                <h2>Reports</h2>
                <Table dataSource={reports} columns={columns} />
            </>
        );
    }

    renderUserData(user, reports) {
        return (
            <>
                <h1>User: {user.email}</h1>
                {this.renderTeamSummary(user)}
                {this.renderReportList(reports)}
            </>
        );
    }

    render() {
        const { isLoading, user, reports } = this.state
        const { workspaceId, userId } = this.props.router.query


        if(userId && isLoading) {
            this.fetchData();
        }

        return (
            <Layout workspaceId={workspaceId}>
                <SecurePage />
                { user ? this.renderUserData(user, reports) : this.renderLoading() }
            </Layout>
        );
    }
}

export default withRouter(withCookies(SingleUserPage));