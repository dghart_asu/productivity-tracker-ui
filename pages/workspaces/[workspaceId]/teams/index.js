import React from 'react';
import { withCookies } from 'react-cookie';
import { withRouter } from 'next/router';

import Layout from '../../../../components/MyLayout';
import SecurePage from '../../../../components/SecurePage';

import TrackerApiClient from '../../../../services/TrackerApiClient';

import {
    Alert as LoadingAlert,
    Button,
    Card,
    Col,
    Empty,
    Form,
    Input,
    Radio,
    Row,
    Spin,
    Switch,
    notification
} from 'antd';

import { UserOutlined } from '@ant-design/icons';

const cardContainerStyle = {
    display: 'flex',
    flexWrap: 'wrap'
}

const cardStyle = {
    width: 300,
    maxWidth: '90%',
    marginRight: '5%',
    marginBottom: '5%'
}

class TeamsPage extends React.Component {
    constructor(props) {
        super(props)

        this.apiClient = new TrackerApiClient(this.props.cookies);

        this.state = {
            teams: [],
            permissions: {
                workspaces: [],
                owner: [],
                team: [],
                manager: []
            },
            isLoading: true
        };
    }

    async componentDidMount() {
        this.fetchData();
    }

    async fetchData() {
        const { workspaceId } = this.props.router.query
        if(!workspaceId) return;

        try {
            const response = await this.apiClient.getTeamsForWorkspace(workspaceId);
            const response2 = await this.apiClient.getPermissions();

            this.setState({
                teams: response.data,
                permissions: response2.data,
                isLoading: false
            });
        } catch (error) {
            this.handleError(error);
        }
    }

    handleError(error) {
        let description = '';
        if(error.response === undefined) {
            description = 'There was an error processing your request: ' + error;
        } else if (error.response.status >= 500) {
            description = 'There was an error while getting teams. Please try again.';
        } else {
            description = error.response.data.message;
        }

        notification.error({
            message: 'Error',
            description: description,
        });
    }

    renderLoading() {
        return (
            <Spin tip="Loading...">
                <LoadingAlert
                    message="Loading"
                    description="Please wait while the data is retrieved."
                    type="info"
                />
            </Spin>
        );
    }

    navToCreateTeam() {
        const { workspaceId } = this.props.router.query;
        this.props.router.push('/workspaces/' + workspaceId + '/teams/create');
    }

    navToUser(userId) {
        const { workspaceId } = this.props.router.query;
        this.props.router.push('/workspaces/' + workspaceId + '/users/' + userId);
    }

    navToTeam(teamId) {
        const { workspaceId } = this.props.router.query;
        this.props.router.push('/workspaces/' + workspaceId + '/teams/' + teamId);
    }

    renderTeamCard(team) {
        const members = team.members.map( m => (
            <li key={m.membershipId}>
                <a onClick={() => this.navToUser(m.userId)}>
                    {m.email}
                </a>
                {m.role == "MANAGER" ? ' (Manager)' : null}
            </li>
        ));

        return (
            <Card
                key={team.id}
                title={team.name}
                extra={<Button type="primary" onClick={() => this.navToTeam(team.id)}>View</Button>}
                style={cardStyle}
            >
                { members.length > 0 ?
                    <ul>{members}</ul> :
                    <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} description="No Users" />

                }
            </Card>
        );
    }

    renderBody() {
        const { teams } = this.state;

        if(teams.length < 1) {
            return <Empty
                image={Empty.PRESENTED_IMAGE_SIMPLE}
                description="There are currently no teams for this workspace"
            />
        }

        return (
            <div style={cardContainerStyle}>
                {teams.map( t => this.renderTeamCard(t))}
            </div>
        );
    }

    renderCreateButton() {
        return (
            <div style={{margin:'10px 0'}}>
                <Button type="primary" onClick={() => this.navToCreateTeam()}>Create Team</Button>
            </div>
        );
    }

    render() {
        const { isLoading, permissions } = this.state;
        const { workspaceId } = this.props.router.query;

        if(workspaceId && isLoading) {
            this.fetchData();
        }

        const isWorkspaceOwner = permissions.owner.indexOf(parseInt(workspaceId)) >= 0;

        return (
            <Layout workspaceId={workspaceId}>
                <SecurePage />
                {isWorkspaceOwner ? this.renderCreateButton() : null }
                <h2>Teams</h2>
                {isLoading ? this.renderLoading() : this.renderBody() }
            </Layout>
        );
    }
}

export default withRouter(withCookies(TeamsPage));