import React from 'react';
import { withCookies } from 'react-cookie';
import { withRouter } from 'next/router';

import TrackerApiClient from '../../../../services/TrackerApiClient';

import Layout from '../../../../components/MyLayout';
import SecurePage from '../../../../components/SecurePage';

import { Alert, Button, Checkbox, Modal, Select, Spin, Table, notification } from 'antd';

import { ExclamationCircleOutlined } from '@ant-design/icons';

const btnStyle = {
    margin: "0 3px"
}

class SingleTeamPage extends React.Component {
    constructor(props) {
        super(props)

        this.apiClient = new TrackerApiClient(this.props.cookies);

        this.state = {
            team: null,
            isLoading: true
        };
    }

    async fetchData() {
        const { teamId } = this.props.router.query
        if(!teamId) return;

        try {
            const response = await this.apiClient.getTeam(teamId);
            const responsePermissions = await this.apiClient.getPermissions();

            response.data.members.sort((a, b) => {
                if (a.name > b.name) return -1;
                if (b.name > a.name) return 1;

                return 0;
            })

            this.setState({
                team: response.data,
                permissions: responsePermissions.data,
                isLoading: false
            });
        } catch (error) {
            this.handleError(error);
        }
    }

    handleError(error) {
        let description = '';
        if(error.response === undefined) {
            description = 'There was an error processing your request: ' + error;
        } else if (error.response.status >= 500) {
            description = 'There was an error while getting the team data. Please try again.';
        } else {
            description = error.response.data.message;
        }

        notification.error({
            message: 'Error',
            description: description,
        });
    }

    navToUser(userId) {
        const { workspaceId } = this.props.router.query;
        this.props.router.push('/workspaces/' + workspaceId + '/users/' + userId);
    }

    showSetManagerModal(teamId, userId) {
        Modal.confirm({
            title: 'Set as Manager',
            content: 'Are you sure you want to set this user as the manager for this team?',
            icon: <ExclamationCircleOutlined />,
            okType: 'danger',
            onOk: (onClose) => { this.handleSetManager(teamId, userId, onClose); }
        });
    }

    async handleSetManager(teamId, userId, onClose) {
        try {
            const response = await this.apiClient.addUserToTeam(teamId, userId, 'MANAGER');
            notification.info({
                message: 'Success',
                description: 'User has been set as the manager of the team',
            });
            this.fetchData();
        } catch (error) {
            this.handleError(error);
        }
        onClose()
    }

    showRemoveUserModal(teamId, userId) {
        Modal.confirm({
            title: 'Remove from Team',
            content: 'Are you sure you want to remove this user from the team?',
            icon: <ExclamationCircleOutlined />,
            okType: 'danger',
            onOk: (onClose) => { this.handleRemoveUser(teamId, userId, onClose); }
        });
    }

    async handleRemoveUser(teamId, userId, onClose) {
        try {
            const response = await this.apiClient.removeUserFromTeam(teamId, userId);
            notification.info({
                message: 'Success',
                description: 'User has been removed from the team',
            });
            this.fetchData();
        } catch (error) {
            this.handleError(error);
        }
        onClose()
    }

    async showDeleteTeamModal(teamId) {
        Modal.confirm({
            title: 'Delete Team',
            content: 'Are you sure you want to remove this team?',
            icon: <ExclamationCircleOutlined />,
            okType: 'danger',
            onOk: (onClose) => { this.handleDeleteTeam(teamId, onClose); }
        });
    }

    async handleDeleteTeam(teamId, onClose) {
        const { workspaceId } = this.props.router.query;

        try {
            const response = await this.apiClient.deleteTeam(teamId);
            notification.info({
                message: 'Success',
                description: 'Team has been deleted from the workspace',
            });
            this.props.router.push('/workspaces/' + workspaceId + '/teams');
        } catch (error) {
            this.handleError(error);
        }
    }

    async showAddToTeamModal(teamId) {
        const { workspaceId } = this.props.router.query

        let userList = [];
        try {
            const response = await this.apiClient.getUsersForWorkspace(workspaceId);
            userList = response.data;
        } catch (error) {
            this.handleError(error);
            return;
        }

        const data = {
            userId: null,
            isManager: false
        }

        const handleSubmit = async (onClose) => {
            if(data.userId) {
                const role = data.isManager ? 'MANAGER' : 'MEMBER';
                try {
                    const response = await this.apiClient.addUserToTeam(teamId, data.userId, role);
                    notification.info({
                        message: 'Success',
                        description: 'User has been added to the team',
                    });
                    onClose()
                    this.fetchData();
                } catch (error) {
                    this.handleError(error);
                }
            } else {
                notification.warning({
                    message: 'Warning',
                    description: 'Please Select a User!',
                });
            }
        }

        Modal.confirm({
            title: 'Add User to Team',
            content: (
                <div>
                    <div style={{marginTop:'10px'}}>
                        <Select onChange={ v => {data.userId = v}} style={{ width: 300 }} placeholder="Select a User">
                            {userList && (
                               userList.map( u => (
                                <Select.Option key={u.id} value={u.id}>{u.email}</Select.Option>
                               ))
                            )}
                        </Select>
                    </div>
                    <div style={{marginTop:'10px'}}>
                        <Checkbox onChange={e => {data.isManager = e.target.checked}}>Assign as Manager</Checkbox>
                    </div>
                </div>

            ),
            okType: 'primary',
            onOk: (onClose) => { handleSubmit(onClose) }
        });
    }

    renderTeam(team, isWorkspaceOwner) {
        const users = team.members;

        const columns = [
          {
            title: 'User',
            dataIndex: 'email',
            key: 'email',
            render: (text, record, index) => {
                return <a onClick={ () => this.navToUser(record.userId) }>{text}</a>
            }
          },
          {
              title: 'Role',
              dataIndex: 'role',
              key: 'role',
              render: (text, record, index) => {
                    const value = (text == 'MANAGER') ? 'Manager' : 'Member';
                  return <span>{value}</span>
              }
            },
        ];

        if(isWorkspaceOwner) {
            columns.push(
                {
                    title: 'Actions',
                    dataIndex: 'date',
                    key: 'actions',
                    render: (text, record, index) => {
                        let managerButton = null;
                        if(record.role != 'MANAGER') {
                            managerButton = <Button
                                style={btnStyle}
                                type="primary"
                                onClick={ () => this.showSetManagerModal(team.id, record.userId)}
                            >
                                Set as Manager
                            </Button>
                        }
                        let removeButton = (
                            <Button
                                style={btnStyle}
                                type="danger"
                                onClick={ () => this.showRemoveUserModal(team.id, record.userId)}
                            >
                                Remove from Team
                            </Button>
                        );

                        return [managerButton, removeButton]
                     }
                  }
            );
        }

        return (
            <>
                <h2>Team: {team.name}</h2>
                <Table dataSource={users} columns={columns} />
            </>
        );
    }

    renderLoading() {
        return (
            <Spin tip="Loading...">
                <Alert
                    message="Loading"
                    description="Please wait while the data is retrieved."
                    type="info"
                />
            </Spin>
        );
    }

    renderOwnerButtons(teamId) {
        return (
            <>
                <Button style={btnStyle} type="primary" onClick={() => this.showAddToTeamModal(teamId)}>Add User</Button>
                <Button style={btnStyle} type="danger" onClick={() => this.showDeleteTeamModal(teamId)}>Delete Team</Button>
            </>
        );
    }

    render() {
        const { isLoading, team, permissions } = this.state
        const { workspaceId, teamId } = this.props.router.query


        if(teamId && isLoading) {
            this.fetchData();
        }

        const isWorkspaceOwner = permissions &&
            permissions.owner.indexOf(parseInt(workspaceId)) >= 0;

        return (
            <Layout workspaceId={workspaceId}>
                {isWorkspaceOwner ? this.renderOwnerButtons(teamId) : null }
                <SecurePage />
                { team ? this.renderTeam(team, isWorkspaceOwner) : this.renderLoading() }
            </Layout>
        );
    }
}

export default withRouter(withCookies(SingleTeamPage));
