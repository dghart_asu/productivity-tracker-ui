import React from 'react';
import { withCookies } from 'react-cookie';
import { withRouter } from 'next/router';

import TrackerApiClient from '../../../../services/TrackerApiClient';

import Layout from '../../../../components/MyLayout';
import SecurePage from '../../../../components/SecurePage';

import { Form, Input, Button, notification } from 'antd';

const formLayout = {
  wrapperCol: { span: 8 },
};

class CreateTeamPage extends React.Component {
    constructor(props) {
        super(props)

        this.apiClient = new TrackerApiClient(this.props.cookies);

        this.state = {
        };
    }

    onCancel() {
        const { workspaceId } = this.props.router.query;
        this.props.router.push('/workspaces/' + workspaceId + '/teams');
    }

    async onSubmit(values) {
        const { workspaceId } = this.props.router.query;

        try {
            const response = await this.apiClient.createTeam(workspaceId, values.name);
            this.props.router.push('/workspaces/' + workspaceId + "/teams");
        } catch (error) {
            this.handleError(error);
        }
    }

    handleError(error) {
        let description = '';
        if(error.response === undefined) {
            description = 'There was an error processing your request: ' + error;
        } else if (error.response.status >= 500) {
            description = 'There was an error while creating the workspace. Please try again.';
        } else {
            description = error.response.data.message;
        }

        notification.error({
            message: 'Error',
            description: description,
        });
    }

    render() {
        const { workspaceId } = this.props.router.query;

        return (
            <Layout workspaceId={workspaceId}>
                <SecurePage />
                <h2>Create New Team</h2>
                <Form
                    {...formLayout}
                    name="createTeam"
                    onFinish={(values) => this.onSubmit(values)}
                >
                    <Form.Item
                        name="name"
                        rules={[{ required: true, message: 'Please enter a name for the team' }]}
                    >
                        <Input placeholder="Team Name" />
                    </Form.Item>

                    <Form.Item>
                        <Button type="primary" htmlType="submit" className="login-form-button">Create</Button>
                        <span> </span>
                        <Button htmlType="button" onClick={ () => this.onCancel() } type="danger">Cancel</Button>
                    </Form.Item>
                </Form>
            </Layout>
        );
    }
}

export default withRouter(withCookies(CreateTeamPage));
