import React from 'react';
import { withCookies } from 'react-cookie';
import { withRouter } from 'next/router';

import TrackerApiClient from '../../../../services/TrackerApiClient';

import SummaryReport from '../../../../components/SummaryReport';
import Layout from '../../../../components/MyLayout';
import SecurePage from '../../../../components/SecurePage';

import { Alert, Spin, notification } from 'antd';


class SingleReportPage extends React.Component {
    constructor(props) {
        super(props)

        this.apiClient = new TrackerApiClient(this.props.cookies);

        this.state = {
            summary: null,
            isLoading: true
        };
    }

    async fetchSummary() {
        const { summaryId } = this.props.router.query
        if(!summaryId) return;

        try {
            const response = await this.apiClient.getSummaryReport(summaryId);
            this.setState({summary:response.data, isLoading: false});
        } catch (error) {
            this.handleError(error);
        }
    }

    handleError(error) {
        let description = '';
        if(error.response === undefined) {
            description = 'There was an error processing your request: ' + error;
        } else if (error.response.status >= 500) {
            description = 'There was an error while getting the summary. Please try again.';
        } else {
            description = error.response.data.message;
        }

        notification.error({
            message: 'Error',
            description: description,
        });
    }

    componentDidMount = async () => {
        this.fetchSummary();
    }

    renderLoading() {
        return (
            <Spin tip="Loading...">
                <Alert
                    message="Loading"
                    description="Please wait while the data is retrieved."
                    type="info"
                />
            </Spin>
        );
    }

    render() {
        const { isLoading, summary } = this.state
        const { workspaceId, summaryId } = this.props.router.query


        if(summaryId && isLoading) {
            this.fetchSummary();
        }

        return (
            <Layout workspaceId={workspaceId}>
                <SecurePage />
                { summaryId ? <SummaryReport summary={summary}/> : this.renderLoading() }
            </Layout>
        );
    }
}

export default withRouter(withCookies(SingleReportPage));