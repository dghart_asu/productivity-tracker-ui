import React from 'react';
import { withCookies } from 'react-cookie';
import { withRouter } from 'next/router';

import TrackerApiClient from '../../../../services/TrackerApiClient';

import ReportStack from '../../../../components/ReportStack';
import Layout from '../../../../components/MyLayout';
import SecurePage from '../../../../components/SecurePage';

import {
    Alert,
    Button,
    DatePicker,
    Form,
    Input,
    Spin,
    Steps,
    notification
} from 'antd';

const { RangePicker } = DatePicker;
const { Step } = Steps;

const stepsContentStyle = {
  marginTop: '16px',
  paddingTop: '20px',
  border: '1px dashed #e9e9e9',
  borderRadius: '2px',
  backgroundColor: '#fafafa',
  minHeight: '200px',
}

const datePickerStyles = {
  textAlign: 'center',
  paddingTop: '60px',
}

const stepsActionStyle = {
  marginTop: '24px',
}

const formLayout = {
  labelCol: { span: 4 },
  wrapperCol: { span: 16 },
};

class CreateSummaryPage extends React.Component {
    constructor(props) {
        super(props)

        this.apiClient = new TrackerApiClient(this.props.cookies);

        this.state = {
            isLoading: true,
            dates: null,
            reports: [],
            current: 0,
        };
    }

    async fetchReports() {
        const { workspaceId } = this.props.router.query
        const { dates } = this.state;

        if(!workspaceId || !dates) return;

        const startDate = dates[0].format('YYYY-MM-DD');
        const endDate = dates[1].format('YYYY-MM-DD');

        try {
            const response = await this.apiClient.getReportsInRange(workspaceId, startDate, endDate);
            const reports = response.data

            // sort most recent to least recent
            reports.sort((a, b) => {
                if (a.date > b.date) return -1;
                if (b.date > a.date) return 1;

                return 0;
            })

            this.setState({
                reports: reports,
                isLoading: false
            });
        } catch (error) {
            this.handleError(error);
        }
    }

    handleError(error) {
        console.log(error);
        let description = '';
        if(error.response === undefined) {
            description = 'There was an error processing your request: ' + error;
        } else if (error.response.status >= 500) {
            description = 'There was an error while getting your reports. Please try again.';
        } else {
            description = error.response.data.message;
        }

        notification.error({
            message: 'Error',
            description: description,
        });
    }

    next() {
        const current = this.state.current + 1;
        this.setState({ current });
    }

    prev() {
        const current = this.state.current - 1;
        this.setState({ current, reports: null, dates: null, notes: null, reportDate: null });
    }

    renderLoading() {
        return (
            <Spin tip="Loading...">
                <Alert
                    message="Loading"
                    description="Please wait while the data is retrieved."
                    type="info"
                />
            </Spin>
        );
    }

    async onSubmit() {
        const { workspaceId } = this.props.router.query;
        const { reportDate, dates, notes } = this.state

        if(!reportDate) {
            notification.error({
                message: 'Error',
                description: 'Please select a date for this report!',
            });
            return;
        }

        const data = {
            workspaceId: workspaceId,
            date: reportDate.format('YYYY-MM-DD'),
            startDate: dates[0].format('YYYY-MM-DD'),
            endDate: dates[1].format('YYYY-MM-DD'),
            publicNotes: notes
        }

        try {
            const response = await this.apiClient.createSummaryReport(data);
            this.props.router.push('/workspaces/' + workspaceId + "/reports");
        } catch (error) {
            this.handleError(error);
        }
    }

    onCancel() {
        const { workspaceId } = this.props.router.query;
        this.props.router.push('/workspaces/' + workspaceId + '/reports');
    }

    renderDatesStep() {
        const { dates } = this.state;

        const onChange = (dates, dateStrings) => {
            this.state.dates = dates;
            this.fetchReports();
        };

        return (
            <div style={datePickerStyles}>
                <h3>Select Date Range for Reports</h3>
                <RangePicker onChange={onChange} />
            </div>
        )
    }

    renderPreviewStep() {
        const { isLoading, reports } = this.state;

        if(isLoading) {
            return this.renderLoading();
        }

        return (
            <>
               <Form
                   {...formLayout}
                   name="createSummary"
               >
                   <Form.Item
                       name="date"
                       label="Date"
                       rules={[{ required: true, message: 'You must choose a date for this report!' }]}
                   >
                       <DatePicker onChange={val => this.setState({reportDate: val})}/>
                   </Form.Item>

                   <Form.Item name="notes" label="Notes">
                       <Input.TextArea
                           rows={4}
                           placeholder="Additional notes for yourself or manager"
                           onChange={ e => this.setState({notes: e.target.value}) }
                       />
                   </Form.Item>

                   <Form.Item label="Reports">
                        <ReportStack reports={reports} />
                   </Form.Item>
                </Form>
           </>
       )
    }

    render() {
        const { current, dates } = this.state
        const { workspaceId } = this.props.router.query

        const steps = [
          {
            title: 'Dates',
            content: this.renderDatesStep(),
          },
          {
            title: 'Preview',
            content: this.renderPreviewStep(),
          },
        ];

        return (
            <Layout workspaceId={workspaceId}>
                <SecurePage />
                <div>
                    <Steps current={current}>
                      {steps.map(item => (
                        <Step key={item.title} title={item.title} />
                      ))}
                    </Steps>
                    <div style={stepsContentStyle}>{steps[current].content}</div>
                    <div style={stepsActionStyle}>
                        {current === 0 && (
                            <Button style={{ margin: 8 }} type="danger" onClick={() => this.onCancel()}>
                                Cancel
                            </Button>
                        )}
                        {current > 0 && (
                            <Button style={{ margin: 8 }} onClick={() => this.prev()}>
                                Previous
                            </Button>
                        )}
                        {current < steps.length - 1 && (
                            dates ?
                            <Button type="primary" onClick={() => this.next()}>Next</Button>
                            :
                            <Button disabled>Next</Button>
                        )}
                        {current === steps.length - 1 && (
                            <Button type="primary" onClick={() => this.onSubmit() }>
                                Create
                            </Button>
                        )}
                    </div>
                </div>
            </Layout>
        );
    }
}

export default withRouter(withCookies(CreateSummaryPage));