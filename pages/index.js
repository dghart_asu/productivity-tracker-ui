import Link from 'next/link'
import Layout from '../components/MyLayout';

import { Button, Result} from 'antd'

export default function Index() {
  return (
    <Layout>
        <Result
            status="warning"
            title="This application is for a school project and is unsupported."
            subTitle="No claims are made about the reliability or usage of the data. Use at your own risk."
            extra={[
                <Link href="/sign_up">
                    <Button type="primary" key="signup">
                        Create Account
                    </Button>
                </Link>,
                <Link href="/login">
                    <Button key="login">
                        Log In
                    </Button>
                </Link>
            ]}
          />
    </Layout>
  );
}