import Layout from '../components/MyLayout';
import UserServiceClient from '../services/UserServiceClient';
import { withCookies } from 'react-cookie';
import { withRouter } from 'next/router'

import { Form, Input, Button, notification } from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';

const formLayout = {
  wrapperCol: { span: 8 },
};

class LogInPage extends React.Component {
    componentDidMount() {
        const { cookies } = this.props;
        let jwt = cookies.get('jwt') ? cookies.get('jwt') : localStorage.getItem("jwt");

        // user is already logged in redirect to the workspaces list
        if(jwt) {
            this.props.router.push('/workspaces')
        }
    }

    onSubmit(values) {
        UserServiceClient.authenticate(values.email, values.password).then(json => {
            // on success
            const { cookies } = this.props;
            cookies.set('jwt', json.jwt, { path: '/' });
            localStorage.setItem("jwt", json.jwt);
            this.props.router.push('/workspaces')
        }).catch(err => {
            // on any failure, display the message
            notification.error({
                message: 'Failed to Log In',
                description: 'There was no user with that email/password combination. Please try again or use the link to reset your password.',
            });
        });
    }

    render() {
        return (
            <Layout>
                <h2>Log In</h2>
                <Form
                    {...formLayout}
                    name="basic"
                    onFinish={(values) => this.onSubmit(values)}
                >
                    <Form.Item
                        name="email"
                        rules={[{ required: true, message: 'Please input your email address!' }]}
                    >
                        <Input prefix={<UserOutlined className="site-form-item-icon" />} placeholder="Username" />
                    </Form.Item>

                    <Form.Item
                        name="password"
                        rules={[{ required: true, message: 'Please enter your password!' }]}
                    >
                        <Input
                          prefix={<LockOutlined className="site-form-item-icon" />}
                          type="password"
                          placeholder="Password"
                        />
                    </Form.Item>

                    <Form.Item>
                        <Button type="primary" htmlType="submit" className="login-form-button">Log in</Button>
                    </Form.Item>


                    <Form.Item {...formLayout}>
                        <p>Forgot your password? <a href="/forgot_password">Click here to reset</a></p>
                    </Form.Item>
                </Form>
            </Layout>
        );
    }
}

export default withRouter(withCookies(LogInPage));
