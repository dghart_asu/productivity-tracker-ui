import Layout from '../components/MyLayout';
import UserServiceClient from '../services/UserServiceClient';
import { withCookies } from 'react-cookie';
import { withRouter } from 'next/router'

import { Form, Input, Button, Modal, notification } from 'antd';
import { LockOutlined } from '@ant-design/icons';

const formLayout = {
  wrapperCol: { span: 8 },
};


class SetPassword extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            token: '',
        };
    }

    componentDidMount() {
        let search = window.location.search;
        let params = new URLSearchParams(search);
        let token = params.get('token');

        this.setState({token: token});
    }

    onSubmit(values) {
        const { token } = this.state;

        console.log('submit', token, values);

        if(!values.password || values.password != values.password2) {
            notification.error({
                message: 'Password Reset',
                description: 'Please enter matching passwords!',
            });
            return;
        }

        UserServiceClient.setPassword(token, values.password).then(json => {
            Modal.info({
                title: 'Success',
                content: (
                    <p>Your password has been successfully changed. Please log in.</p>
                ),
                okType: 'primary',
                onOk: (onClose) => {
                    onClose();
                    this.props.router.push('/login');
                }
            });

        }).catch( error => {
            const msg = error.message ? error.message : 'There was an error processing your request. Please try again.';

            notification.error({
                message: 'Error',
                description: msg,
            });
        });
    }

    render() {
        return (
            <Layout>
                <h2>Set Password</h2>
                <Form
                    {...formLayout}
                    name="setPassword"
                    onFinish={(values) => this.onSubmit(values)}
                >
                    <Form.Item
                        name="password"
                        rules={[{ required: true, message: 'Please enter a new password with at least 4 characters!' }]}
                    >
                        <Input.Password prefix={<LockOutlined className="site-form-item-icon" />} placeholder="New Password" />
                    </Form.Item>

                    <Form.Item
                            name="password2"
                            rules={[{ required: true, message: 'Please enter the same password to confirm' }]}
                        >
                            <Input.Password prefix={<LockOutlined className="site-form-item-icon" />} placeholder="Confirm Password" />
                        </Form.Item>

                    <Form.Item>
                        <Button type="primary" htmlType="submit" className="login-form-button">Set Password</Button>
                    </Form.Item>

                </Form>
            </Layout>
        );
    }
}

export default withRouter(SetPassword);
