import React from 'react';
import { withCookies } from 'react-cookie';
import { withRouter } from 'next/router';

import Layout from '../../components/MyLayout';
import SecurePage from '../../components/SecurePage';

import TrackerApiClient from '../../services/TrackerApiClient';

import { Heading, PtInput, AlertBanner, Alert, StyledPtButton} from '../../components';

import {
    Alert as LoadingAlert,
    Button,
    Form,
    Input,
    Radio,
    Spin,
    Switch,
    notification
} from 'antd';

const formLayout = {
  wrapperCol: { span: 8 },
};

const switchLabelStyle = {
  marginLeft: '10px',
};



class AccountPage extends React.Component {
    constructor(props) {
        super(props);

        this.apiClient = new TrackerApiClient(this.props.cookies);

        this.state = {
            isLoading: true,
            notifications: false
        }
    }

    async componentDidMount() {
        try {
            const response = await this.apiClient.getSettings();
            this.setState({notifications: response.data.notifications, isLoading: false});
        } catch (error) {
            this.handleErrorResponse(error);
        }
    }

    handleErrorResponse(error) {
        if (error.response.status >= 500) {
            notification.error({
                message: 'Server Error',
                description: 'There was an error with the application. Please try again.',
            });
        } else {
            // on any failure, display the message
            notification.error({
                message: 'Error',
                description: error.response.data.message,
            });
        }
    }

    onCancel() {
        this.props.router.push('/workspaces')
    }

    beforeSubmit(one, two, three) {
        console.log('before submit', one, two, three);
    }

    async onSubmit(values) {
        const { password, newPassword, confirmPassword } = values;
        const { notifications } = this.state;

        try {
            const response = await this.apiClient.updateSettings(password, confirmPassword, notifications);
            notification.info({
                message: 'Settings Updated',
                description: "Your account settings have been saved!",
            });
        } catch (error) {
            this.handleErrorResponse(error);
        }
    }

    renderLoading() {
        return (
            <Spin tip="Loading...">
                <LoadingAlert
                    message="Loading"
                    description="Please wait while the data is retrieved."
                    type="info"
                />
            </Spin>
        );
    }



    renderForm() {
        const { notifications } = this.state;
        const noteVal = this.state.notifications;

        return (
            <Form
                {...formLayout}
                name="settings"
                onFinish={(values) => this.onSubmit(values)}
            >
                <h3>Change Password</h3>
                <p>(Leave blank to keep the same password)</p>
                <Form.Item name="password">
                    <Input
                      type="password"
                      placeholder="Old Password"
                    />
                </Form.Item>

                <Form.Item name="newPassword">
                    <Input
                      type="password"
                      placeholder="New Password"
                    />
                </Form.Item>

                <Form.Item
                    name="confirmPassword"
                    dependencies={['newPassword']}
                    rules={[
                      ({ getFieldValue }) => ({
                        validator(rule, value) {
                          if (!value || getFieldValue('newPassword') === value) {
                            return Promise.resolve();
                          }
                          return Promise.reject('The two passwords that you entered do not match!');
                        },
                      }),
                    ]}
                >
                    <Input
                      type="password"
                      placeholder="Confirm New Password"
                    />
                </Form.Item>

                <h3>Notification Settings</h3>

                <Form.Item>
                    <Switch name="notifications" defaultChecked={noteVal} onChange={(b) => { this.setState({notifications: b}) }}>
                    </Switch>
                    <span style={switchLabelStyle}>Receive Email Notifications</span>
                </Form.Item>

                <Form.Item>
                    <Button type="primary" htmlType="submit" className="login-form-button">Save</Button>
                    <span> </span>
                    <Button htmlType="button" onClick={ () => this.onCancel() } type="danger">Cancel</Button>
                </Form.Item>
            </Form>
        );
    }

    render() {
        const { isLoading } = this.state;

        return (
            <Layout>
                <SecurePage />
                <h2>Account Settings</h2>
                {isLoading ? this.renderLoading() : this.renderForm() }
            </Layout>
        );
    }
}

export default withRouter(withCookies(AccountPage));