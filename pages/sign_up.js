import Link from 'next/link'
import Layout from '../components/MyLayout';
import UserServiceClient from '../services/UserServiceClient';
import { withCookies } from 'react-cookie';
import { withRouter } from 'next/router'

import { Button, Form, Input, Modal, notification } from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';

const formLayout = {
  wrapperCol: { span: 8 },
};

class SignUpPage extends React.Component {
    onSubmit(values) {
        UserServiceClient.register(values.firstName, values.lastName, values.email).then(json => {
            // on success
            Modal.info({
                title: 'Success',
                content: (
                    <p>
                        Thank you for signing up. Please check your email for a confirmation link.
                        You will be able to set a new password, then follow the link below to sign in.
                    </p>
                ),
                okType: 'primary',
                onOk: (onClose) => {
                    onClose();
                    this.props.router.push('/login');
                }
            });
        }).catch( error => {
            const msg = error.message ? error.message : 'There was an error processing your request. Please try again.';

            notification.error({
                message: 'Error',
                description: msg,
            });
        });
    }

    render() {
        return (
            <Layout>
                <h2>Register</h2>
                <Form
                    {...formLayout}
                    name="register"
                    onFinish={(values) => this.onSubmit(values)}
                >
                    <Form.Item
                        name="firstName"
                        rules={[{ required: true, message: 'First name cannot be blank!' }]}
                    >
                        <Input placeholder="First Name" />
                    </Form.Item>

                    <Form.Item
                        name="lastName"
                        rules={[{ required: true, message: 'Last name cannot be blank!' }]}
                    >
                        <Input placeholder="Last Name" />
                    </Form.Item>

                    <Form.Item
                        name="email"
                        rules={[{ required: true, message: 'Email must be valid!' }]}
                    >
                        <Input placeholder="Email Address" />
                    </Form.Item>

                    <Form.Item>
                        <Button type="primary" htmlType="submit" className="login-form-button">Finish Sign Up</Button>
                    </Form.Item>

                    <Form.Item {...formLayout}>
                        <p>Already have an account? <a href="/login">Click here to log in</a></p>
                    </Form.Item>
                </Form>
            </Layout>
        );
    }
}

export default withRouter(SignUpPage);