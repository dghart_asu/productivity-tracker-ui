import Layout from '../components/MyLayout';
import UserServiceClient from '../services/UserServiceClient';
import { withCookies } from 'react-cookie';
import { withRouter } from 'next/router'

import { Form, Input, Button, Modal, notification } from 'antd';
import { UserOutlined } from '@ant-design/icons';

const formLayout = {
  wrapperCol: { span: 8 },
};

class ForgotPassword extends React.Component {
    onSubmit(values) {
        UserServiceClient.resetPassword(values.email).then(json => {
            // on success
            Modal.info({
                title: 'Success',
                content: (
                    <p>You will receive an email to reset your password if a user with this email address exists.</p>
                ),
                okType: 'primary',
                onOk: (onClose) => {
                    onClose();
                    this.props.router.push('/login');
                }
            });
        }).catch( error => {
            const msg = error.message ? error.message : 'There was an error processing your request. Please try again.';

            notification.error({
                message: 'Error',
                description: msg,
            });
        });
    }

    render() {
        return (
            <Layout>
                <h2>Reset Password</h2>
                <Form
                    {...formLayout}
                    name="resetPassword"
                    onFinish={(values) => this.onSubmit(values)}
                >
                    <Form.Item
                        name="email"
                        rules={[{ required: true, message: 'Please input your email address!' }]}
                    >
                        <Input prefix={<UserOutlined className="site-form-item-icon" />} placeholder="Email Address" />
                    </Form.Item>

                    <Form.Item>
                        <Button type="primary" htmlType="submit" className="login-form-button">Reset Password</Button>
                    </Form.Item>

                </Form>
            </Layout>
        );
    }
}

export default withRouter(ForgotPassword);

