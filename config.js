const env = 'development'

const configs = {
  development: {
    apiRoot: 'http://localhost:8080/',
    trackerApiRoot: 'http://localhost:8081/'

  },
  production: {
    apiRoot: 'https://prod-tracker.com/',
    trackerApiRoot: 'https://prod-tracker.com/',
  },
}[env];

export default configs;